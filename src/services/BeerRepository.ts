// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import axios from "axios";
import { Beer } from "@/models/Beer";

const apiClient = axios.create({
  baseURL: "https://api.punkapi.com/v2/",
});

class BeerRepository {
  async getBeers(page: number, itemsPerPage: number): Promise<Beer[]> {
    const { data } = await apiClient.get(
      `beers?page=${page}&per_page=${itemsPerPage}`
    );
    return Promise.resolve(data);
  }
}

export default new BeerRepository();
