import { InjectionKey } from "vue";
import { createStore, Store, useStore as baseUseStore } from "vuex";
import { Beer } from "@/models/Beer";
import BeerRepository from "@/services/BeerRepository";

export interface State {
  beers: Beer[];
  selectedBeerId: number | null;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    beers: [],
    selectedBeerId: null,
  },

  mutations: {
    SET_BEERS(state, beers) {
      state.beers = beers;
    },

    SET_SELECTED_BEER_ID(state, id) {
      state.selectedBeerId = id;
    },
  },

  actions: {
    async fetchBeers({ commit }, { page, itemsPerPage }): Promise<Beer[]> {
      const beers = await BeerRepository.getBeers(page, itemsPerPage);
      commit("SET_BEERS", beers);
      return Promise.resolve(beers);
    },

    setSelectedBeerId({ commit }, id) {
      commit("SET_SELECTED_BEER_ID", id);
    },
  },

  getters: {
    getSelectedBeer: (state) => {
      return state.beers.find((x) => x.id === state.selectedBeerId);
    },
  },
});

export function useStore(): Store<State> {
  return baseUseStore(key);
}
